# Lua test

Ce projet permet de créer un workspace gitpod capable de réaliser des tests unitaires.

Pour cela il utilise une image héhergée chez github.

https://github.com/Akanoa/gitpod-luaunit


## Lancer les tests

```bash
lua test.lua
```
